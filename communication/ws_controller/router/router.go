package router

import (
	"fmt"
	"net/http"

	logdef "measurement-backend/common/logdef"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"

	handlers "measurement-backend/communication/ws_controller/handlers"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func Upgrader(rw http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	fmt.Println("upgrade the connection...")
	conn, err := upgrader.Upgrade(rw, r, nil)
	if err != nil {
		logdef.Log.Println(err)
		return conn, err
	}
	return conn, nil
}

func chatHanlder(responseWriter http.ResponseWriter, request *http.Request, hub *handlers.Hub) {

	username := mux.Vars(request)["username"]

	// username := "van"

	connection, err := Upgrader(responseWriter, request)
	if err != nil {
		logdef.Log.Println(err)
		return
	}

	handlers.CreateNewSocketUser(hub, connection, username)
}

// func cpuServerHandler(w http.ResponseWriter, r *http.Request) {

// connection, err := Upgrader(w, r)
// if err != nil {
// 	logdef.Log.Println(err)
// 	return
// }
// connection.WriteMessage()
// }

func AppRoutes(route *mux.Router) {

	logdef.Log.Println("Loadeding Routes...")

	hub := handlers.NewHub()

	// hub具有注册与注销的功能
	go hub.Run()

	route.HandleFunc("", func(responseWriter http.ResponseWriter, request *http.Request) {
		chatHanlder(responseWriter, request, hub)
	})

	// route.HandleFunc("/ws", func(responseWriter http.ResponseWriter, request *http.Request)) {

	// }

	logdef.Log.Println("Routes are Loaded")
}
