package router

import (
	"encoding/json"
	logdef "measurement-backend/common/logdef"
	"net/http"
)

type websocketsResult struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func WebsocketApiHandler(w http.ResponseWriter, r *http.Request) {

	// defer r.Body.Close()

	// path := r.URL.Path
	// // 通过path寻找controller
	// config, ok := WebsocketsApiConfig[path]
	// if !ok {
	// 	logdef.Log.Errorf("no such path error: %s", path)
	// }

	// // temp := controller.MesGetController()
	// controller := config.MesGetController()
	// controller.MesGetResult(w, r)

	// SendResult(w, 0, "test", "test")

}

func SendResult(w http.ResponseWriter, code int, msg string, data interface{}) {

	resp := websocketsResult{
		code,
		msg,
		data,
	}

	body, err := json.Marshal(&resp)
	if err != nil {
		logdef.Log.Errorf("StoreHttpResponseMarshal: error = %s", err)
	}
	_, err = w.Write(body)
	if err != nil {
		logdef.Log.Errorf("StoreHttpResponseMarshal: error = %s", err)
	}
}
