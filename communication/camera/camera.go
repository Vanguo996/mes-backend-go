package camera

import (
	"bytes"
	"flag"
	"fmt"
	"image"
	"image/jpeg"
	"measurement-backend/common/logdef"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"os"
	"sort"
	"strconv"
	"time"

	"github.com/blackjack/webcam"
)

const (
	V4L2_PIX_FMT_PJPG = 0x47504A50
	V4L2_PIX_FMT_YUYV = 0x56595559
)

type FrameSizes []webcam.FrameSize

func (slice FrameSizes) Len() int {
	return len(slice)
}

//For sorting purposes
func (slice FrameSizes) Less(i, j int) bool {
	ls := slice[i].MaxWidth * slice[i].MaxHeight
	rs := slice[j].MaxWidth * slice[j].MaxHeight
	return ls < rs
}

//For sorting purposes
func (slice FrameSizes) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

var supportedFormats = map[webcam.PixelFormat]bool{
	V4L2_PIX_FMT_PJPG: true,
	V4L2_PIX_FMT_YUYV: true,
}

func CameraDemo() {

	dev := flag.String("d", "/dev/video0", "video device to use")
	// 视频格式
	fmtstr := flag.String("f", "", "video format to use, default first supported")
	// 选择视频大小
	szstr := flag.String("s", "", "frame size to use, default largest one")
	//选择模式
	single := flag.Bool("m", false, "single image http mode, default mjpeg video")

	addr := flag.String("l", ":8080", "addr to listien")

	fps := flag.Bool("p", false, "print fps info")
	flag.Parse()

	// 打开摄像头
	cam, err := webcam.Open(*dev)
	if err != nil {
		panic(err.Error())
	}
	defer cam.Close()

	//
	// SelectFromSize(cam, fmtstr, szstr)

	format_desc := cam.GetSupportedFormats()

	// 列出所有的格式
	fmt.Println("Available formats:")
	for _, s := range format_desc {
		fmt.Fprintln(os.Stderr, s)
	}

	var format webcam.PixelFormat

FMT:
	for f, s := range format_desc {
		if *fmtstr == "" {
			if supportedFormats[f] {
				format = f
				break FMT
			}
		} else if *fmtstr == s {
			if !supportedFormats[f] {
				logdef.Log.Println(format_desc[f], "format is not supported, exiting")
				return
			}
			format = f
			break
		}
	}

	if format == 0 {
		logdef.Log.Println("No format found, exiting")
		return
	}
	// select frame size
	frames := FrameSizes(cam.GetSupportedFrameSizes(format))
	sort.Sort(frames)

	//输出适合
	//Supported frame sizes for format YUV 4:2:2 (YUYV)
	fmt.Fprintln(os.Stderr, "Supported frame sizes for format", format_desc[format])
	for _, f := range frames {
		fmt.Fprintln(os.Stderr, f.GetString())
	}
	// 选择视频大小
	var size *webcam.FrameSize
	if *szstr == "" {
		//
		size = &frames[len(frames)-3]
	} else {
		fmt.Println(*szstr)
		for _, f := range frames {
			if *szstr == f.GetString() {
				size = &f
			}
		}
	}

	if size == nil {
		logdef.Log.Println("No matching frame size, exiting")
		return
	}

	fmt.Fprintln(os.Stderr, "Requesting", format_desc[format], size.GetString())
	f, w, h, err := cam.SetImageFormat(format, uint32(size.MaxWidth), uint32(size.MaxHeight))
	if err != nil {
		logdef.Log.Println("SetImageFormat return error", err)
		return
	}

	fmt.Fprintf(os.Stderr, "Resulting image format: %s %dx%d\n", format_desc[f], w, h)

	// start streaming
	err = cam.StartStreaming()
	if err != nil {
		logdef.Log.Println(err)
		return
	}

	var (
		li   chan *bytes.Buffer = make(chan *bytes.Buffer)
		fi   chan []byte        = make(chan []byte)
		back chan struct{}      = make(chan struct{})
	)

	go encodeToImage(cam, back, fi, li, w, h, f)

	logdef.Log.Info(*single)
	if *single {
		go httpImage(*addr, li)
	} else {
		go httpVideo(*addr, li)
	}

	// go httpVideo(*addr, li)

	timeout := uint32(60) //5 seconds
	start := time.Now()
	var fr time.Duration

	for {
		logdef.Log.Info("waiting for frame...")
		err = cam.WaitForFrame(timeout)
		if err != nil {
			logdef.Log.Println(err)
			return
		}

		switch err.(type) {
		case nil:
		case *webcam.Timeout:
			logdef.Log.Println(err)
			continue
		default:
			logdef.Log.Println(err)
			return
		}

		frame, err := cam.ReadFrame()
		if err != nil {
			logdef.Log.Println(err)
			return
		}
		if len(frame) != 0 {

			// print framerate info every 10 seconds
			fr++
			if *fps {
				if d := time.Since(start); d > time.Second*10 {
					fmt.Println(float64(fr)/(float64(d)/float64(time.Second)), "fps")
					start = time.Now()
					fr = 0
				}
			}
			select {
			case fi <- frame:
				<-back
			default:
			}
		}
	}

}

// 选择视频分辨率 fmtstr
func SelectFromSize(cam *webcam.Webcam, fmtstr *string, szstr *string) {
	format_desc := cam.GetSupportedFormats()

	// 列出所有的格式
	fmt.Println("Available formats:")
	for _, s := range format_desc {
		fmt.Fprintln(os.Stderr, s)
	}

	var format webcam.PixelFormat

FMT:
	for f, s := range format_desc {
		if *fmtstr == "" {
			if supportedFormats[f] {
				format = f
				break FMT
			}
		} else if *fmtstr == s {
			if !supportedFormats[f] {
				logdef.Log.Println(format_desc[f], "format is not supported, exiting")
				return
			}
			format = f
			break
		}
	}
	if format == 0 {
		logdef.Log.Println("No format found, exiting")
		return
	}
	// select frame size
	frames := FrameSizes(cam.GetSupportedFrameSizes(format))
	sort.Sort(frames)

	//输出适合
	//Supported frame sizes for format YUV 4:2:2 (YUYV)
	fmt.Fprintln(os.Stderr, "Supported frame sizes for format", format_desc[format])
	for _, f := range frames {
		fmt.Fprintln(os.Stderr, f.GetString())
	}
	var size *webcam.FrameSize
	if *szstr == "" {
		size = &frames[len(frames)-1]
	} else {
		for _, f := range frames {
			if *szstr == f.GetString() {
				size = &f
			}
		}
	}
	if size == nil {
		logdef.Log.Println("No matching frame size, exiting")
		return
	}

	fmt.Fprintln(os.Stderr, "Requesting", format_desc[format], size.GetString())
	f, w, h, err := cam.SetImageFormat(format, uint32(size.MaxWidth), uint32(size.MaxHeight))
	if err != nil {
		logdef.Log.Println("SetImageFormat return error", err)
		return

	}
	fmt.Fprintf(os.Stderr, "Resulting image format: %s %dx%d\n", format_desc[f], w, h)
}

func httpVideo(addr string, li chan *bytes.Buffer) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		logdef.Log.Println("connect from", r.RemoteAddr, r.URL)
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			return
		}

		//remove stale image
		<-li
		const boundary = `frame`
		w.Header().Set("Content-Type", `multipart/x-mixed-replace;boundary=`+boundary)
		multipartWriter := multipart.NewWriter(w)
		multipartWriter.SetBoundary(boundary)
		for {
			img := <-li
			image := img.Bytes()
			iw, err := multipartWriter.CreatePart(textproto.MIMEHeader{
				"Content-type":   []string{"image/jpeg"},
				"Content-length": []string{strconv.Itoa(len(image))},
			})
			if err != nil {
				logdef.Log.Println(err)
				return
			}
			_, err = iw.Write(image)
			if err != nil {
				logdef.Log.Println(err)
				return
			}
		}
	})

	logdef.Log.Fatal(http.ListenAndServe(addr, nil))
}

func httpImage(addr string, li chan *bytes.Buffer) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		logdef.Log.Println("connect from", r.RemoteAddr, r.URL)
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			return
		}

		//remove stale image
		<-li

		img := <-li

		w.Header().Set("Content-Type", "image/jpeg")

		if _, err := w.Write(img.Bytes()); err != nil {
			logdef.Log.Println(err)
			return
		}

	})

	logdef.Log.Fatal(http.ListenAndServe(addr, nil))
}

func encodeToImage(wc *webcam.Webcam, back chan struct{}, fi chan []byte, li chan *bytes.Buffer, w, h uint32, format webcam.PixelFormat) {

	var (
		frame []byte
		img   image.Image
	)
	for {
		bframe := <-fi
		// copy frame
		if len(frame) < len(bframe) {
			frame = make([]byte, len(bframe))
		}
		copy(frame, bframe)
		back <- struct{}{}

		switch format {
		case V4L2_PIX_FMT_YUYV:
			yuyv := image.NewYCbCr(image.Rect(0, 0, int(w), int(h)), image.YCbCrSubsampleRatio422)
			for i := range yuyv.Cb {
				ii := i * 4
				yuyv.Y[i*2] = frame[ii]
				yuyv.Y[i*2+1] = frame[ii+2]
				yuyv.Cb[i] = frame[ii+1]
				yuyv.Cr[i] = frame[ii+3]

			}
			img = yuyv
		default:
			logdef.Log.Fatal("invalid format ?")
		}
		//convert to jpeg
		buf := &bytes.Buffer{}
		if err := jpeg.Encode(buf, img, nil); err != nil {
			logdef.Log.Fatal(err)
			return
		}

		const N = 50
		// broadcast image up to N ready clients
		nn := 0
	FOR:
		for ; nn < N; nn++ {
			select {
			case li <- buf:
			default:
				break FOR
			}
		}
		if nn == 0 {
			li <- buf
		}
	}
}
