package grpc_controller

import (
	"context"
	"log"
	pb "measurement-backend/pb"
)

// 关闭指定仪器的输入
func CloseOutputInstruments(client pb.InstrumentsControllerClient, resourceName string) {

	req_close_outp := &pb.Request{InstrumentsName: resourceName}

	response, err := client.CloseOutput(context.Background(), req_close_outp)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(response.Result)
}

// 执行扫描任务
func SweepModeStart(client pb.InstrumentsControllerClient,
	StartVolt, VoltStep, EndVolt, CurrentCmpl, MesSpeed float32) {

	req := &pb.SweepRequest{
		StartVolt:   StartVolt,
		VoltStep:    VoltStep,
		EndVolt:     EndVolt,
		CurrentCmpl: CurrentCmpl,
		MesSpeed:    MesSpeed,
	}

	res, err := client.IVSweepMode(context.Background(), req)
	if err != nil {
		log.Fatal(err)
	}
	for {
		word, err := res.Recv()
		if err != nil {
			log.Fatal(err)
			break
		}

		log.Println(word)
		// time.Sleep(10 * time.Millisecond)
	}

	// log.Println(res.Recv())
}

func SimulationModeStart(client pb.InstrumentsControllerClient) {
	req := &pb.SimRequest{
		StartVolt: 0.2,
	}

	res, err := client.SimulationMode(context.Background(), req)

	if err != nil {
		log.Fatal(err)
	}

	for {
		word, err := res.Recv()
		if err != nil {
			log.Fatal(err)
			break
		}

		log.Println(word)
		// time.Sleep(10 * time.Millisecond)
	}

}
