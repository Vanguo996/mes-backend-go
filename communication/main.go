package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"google.golang.org/grpc"

	logdef "measurement-backend/common/logdef"
	camera "measurement-backend/communication/camera"
	constant "measurement-backend/constant"
	// router "measurement-backend/communication/ws_controller/router"
)

func test() {

	conn, err := grpc.Dial(constant.HostAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	// 新建一个仪器控制器
	// client := pb.NewInstrumentsControllerClient(conn)

	// grpc_controller.CloseOutputInstruments(client, constant.Instr2400)

	// go grpc_controller.SweepModeStart(client, -2, 0.1, 2, 100e-3, 1)

	// go grpc_controller.SimulationModeStart(client)

	logdef.Log.Infof("websockets server started on %s", constant.WebsocketServerPort)

	route := mux.NewRouter()

	// router.AppRoutes(route)

	// for key := range router.WebsocketsApiConfig {
	// 	route.HandleFunc(key, router)
	// }

	logdef.Log.Fatal(http.ListenAndServe(constant.WebsocketServerPort, route))
}

func main() {
	camera.CameraDemo()
}
