package main

import (
	db "measurement-backend/common/db"
	"measurement-backend/constant"

	"measurement-backend/common/logdef"
	routes "measurement-backend/login/routes"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {

	// 初始化日志
	// exepath, _ := os.Executable()
	// exedir := filepath.Dir(exepath)

	if db.GetMesClient() == nil {
		logdef.Log.Errorf("db init failed")
		return
	}

	router := mux.NewRouter()

	// 定义需要鉴权的接口
	// new_router := router.PathPrefix("/api/getUser").Subrouter()
	// new_router.Use(api.CheckToken)
	
// 加载普通接口
	for key := range routes.APIControllerConfig {
		logdef.Log.Infof("loading router: %s", key)
		router.HandleFunc(key, routes.APICommonHandle)
	}

	// 加载websockets接口
	for key := range routes.WebsocketsApiConfig {
		logdef.Log.Infof("loading websocket api: %s", key)
		router.HandleFunc(key, routes.WebsocketApiHandler)
	}

	// 添加Access-Control-Allow-Credentials
	credentials := handlers.AllowCredentials()

	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type"})

	methods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	origins := handlers.AllowedOrigins([]string{"*"})

	logdef.Log.Infof("login server started on %s", constant.LoginServerPort)

	// cors避免前端错误
	if err := http.ListenAndServe(constant.LoginServerPort, handlers.CORS(credentials, methods, origins, headers)(router)); err != nil {
		logdef.Log.Fatal(err)
	}
}
