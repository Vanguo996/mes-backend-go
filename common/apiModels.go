package common

import (
	"net/http"

	"github.com/gorilla/websocket"
)

type ApiInterface interface {
	StoreRequest() error
	Valid() error
	GetResult() (interface{}, error)
}

type ApiController struct {
	GetController func() ApiInterface
}

type ApiResult struct {
	Code int         `json:"code"`           // 返回状态码，0成功，非0异常
	Msg  string      `json:"msg"`            // 返回信息
	Data interface{} `json:"data,omitempty"` // 返回数据(按需：int,string,object,array等)
}

type MesApiInterface interface {
	MesGetResult(w http.ResponseWriter, r *http.Request) (interface{}, error)
}

// 通用功能控制器
type MesApiController struct {
	MesGetController func() MesApiInterface
}

// websockets接口
type WebsocketsInterface interface {
	WsGetResult(connection *websocket.Conn) (interface{}, error)
}

// websockets控制器
type WebsocketsApiController struct {
	WebsocketsController func() WebsocketsInterface
}
