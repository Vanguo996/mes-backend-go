package db

import (
	"fmt"
	"measurement-backend/common/logdef"
	"time"

	// "gorm.io/driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	// "gorm.io/gorm"
)

// const (
// 	dsn = "root:reactgo@tcp(127.0.0.1:3306)/mes?charset=utf8mb4&parseTime=True"
// )

var (
	mesClient *gorm.DB //mes连接池

	DbConfig = &MysqlConfig{
		Host:     "csumeslab.top",
		Port:     7890,
		User:     "root",
		Pwd:      "mes@2021",
		Database: "measurement_system",
		OpenLog:  true,
	}
)

type MysqlConfig struct {
	Host        string
	Port        int
	User        string
	Pwd         string
	Database    string
	MaxOpenConn int           // 连接池最大连接数
	MaxIdleConn int           // 最大空闲连接数，表示即使没有redis连接时依然可以保持N个空闲的连接，而不被清除，随时处于待命状态
	MaxLifetime time.Duration // 单个连接最大生命周期
	OpenLog     bool          // 是否打开sql日志
}

// 初始化db
func init() {
	mesClient = getDbClient(mesClient, DbConfig)
}

func GetMesClient() *gorm.DB {

	mesClient = getDbClient(mesClient, DbConfig)
	mesClient.SingularTable(true) //禁用复数表名
	return mesClient
}

// 初始化db链接
func getDbClient(db *gorm.DB, mysqlConfig *MysqlConfig) *gorm.DB {
	if db != nil {
		return db
	}
	logdef.Log.Infof("初始化db")
	// 链接数据源
	dbSource := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		mysqlConfig.User,
		mysqlConfig.Pwd,
		mysqlConfig.Host,
		mysqlConfig.Port,
		mysqlConfig.Database,
	)
	instanceDb, err := gorm.Open("mysql", dbSource)
	if err != nil {
		logdef.Log.Errorf("failed to instance db. error = ", err)
		return nil
	}

	if mysqlConfig.MaxOpenConn == 0 {
		mysqlConfig.MaxOpenConn = 600
	}
	if mysqlConfig.MaxIdleConn == 0 {
		mysqlConfig.MaxIdleConn = 30
	}
	if mysqlConfig.MaxLifetime == 0 {
		mysqlConfig.MaxLifetime = time.Second * 90
	}

	instanceDb.DB().SetMaxOpenConns(mysqlConfig.MaxOpenConn)    // 连接池最大连接数量
	instanceDb.DB().SetMaxIdleConns(mysqlConfig.MaxIdleConn)    // 最多保留空闲连接数
	instanceDb.DB().SetConnMaxLifetime(mysqlConfig.MaxLifetime) // 连接最大生命周期
	err = instanceDb.DB().Ping()
	if err != nil {
		logdef.Log.Errorf("ping DB ERROR:%v", err)
		return nil
	}
	logdef.Log.Info("成功连接mysql")

	// 设置sql日志
	SetLog(instanceDb, mysqlConfig.OpenLog)

	return instanceDb

}

func SetLog(db *gorm.DB, openLog bool) {
	db.LogMode(openLog)
}
