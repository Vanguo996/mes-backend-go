package common

import (
	"encoding/json"
	"io/ioutil"
	"measurement-backend/common/conf"
	"net/http"
	"runtime/debug"
	"strings"

	"github.com/go-playground/validator"
	// log "github.com/sirupsen/logrus"
	"measurement-backend/common/logdef"
)

// 异常处理
func HandleError(w http.ResponseWriter) {
	if r := recover(); r != nil {
		logdef.Log.Errorf("panic info:\n%s\n", debug.Stack())
		SendResult(w, conf.SerErr, "panic", r)
	}
}

// 发送结果
func SendResult(w http.ResponseWriter, code int, msg string, data interface{}) {
	resp := ApiResult{
		code,
		msg,
		data,
	}

	WriteToHTTPResponse(w, &resp)
}

// 将结果写入http响应中
func WriteToHTTPResponse(w http.ResponseWriter, result interface{}) {
	body, err := json.Marshal(result)
	if err != nil {
		logdef.Log.Errorf("StoreHttpResponseMarshal: error = %s", err)
	}
	_, err = w.Write(body)
	if err != nil {
		logdef.Log.Errorf("StoreHttpResponseMarshal: error = %s", err)
	}
}

// 解析http请求
func StoreRequestBody(r *http.Request, body interface{}) error {

	// if r.Method == http.MethodPost {
	//Content-type = json

	ct := r.Header.Get("Content-Type")
	if !strings.Contains(ct, "json") {
		logdef.Log.Errorf("unsupported content-type. type = %s", ct)
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logdef.Log.Errorf("StoreRequestBody.ReadAll: error = %s", err)
		return err
	}

	logdef.Log.Infof("StoreRequestBody.reqBody = %s", string(reqBody))

	if err := json.Unmarshal(reqBody, body); err != nil {
		logdef.Log.Errorf("StoreRequestBody.Unmarshal: error = %s", err)
		return err
	}

	return nil

	// } else {
	// 	err := fmt.Errorf("StoreRequestBody: illegal method = %s", r.Method)
	// 	logdef.Log.Errorf(err.Error())
	// 	return err
	// }
}

//校验参数
func ValidParam(in interface{}) error {
	vd := validator.New()
	return vd.Struct(in)

}
