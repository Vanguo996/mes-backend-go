package models

/******sql******
CREATE TABLE `user_info_test` (
  `user_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户数据'
******sql******/
// UserInfoTest 用户数据
type UserInfoTest struct {
	UserName string `gorm:"column:user_name"`
	Email    string `gorm:"column:email"`
	UserID   int    `gorm:"primaryKey;column:user_id"`
	Password string `gorm:"column:password"`
}
