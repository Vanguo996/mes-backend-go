package logdef

import (
	"github.com/sirupsen/logrus"
)

const (
	Log_Nil = iota
	Log_Debug
	Log_Info
	Log_Error
	Log_Any
	Log_Max
)

var Log = logrus.New()

func init() {
	Log.SetLevel(logrus.DebugLevel)

	Log.SetFormatter(&logrus.TextFormatter{
		TimestampFormat:           "2006-01-02 15:04:05",
		ForceColors:               true,
		EnvironmentOverrideColors: true,
		FullTimestamp:             true,
	})
	Log.SetReportCaller(true)
}

// func Info(a ...interface{}) {
// 	outputLog(fmt.Sprint(a), Log_Info, 0)
// }

// func Infof(format string, a ...interface{}) {
// 	if len(a) == 0 {
// 		outputLog(fmt.Sprint(format), Log_Info, 0)
// 	} else {
// 		outputLog(fmt.Sprintf(format, a...), Log_Info, 0)
// 	}
// }

// func Error(a ...interface{}) {
// 	outputLog(fmt.Sprint(a...), Log_Error, 0)
// }

// func Errorf(format string, a ...interface{}) {
// 	if len(a) == 0 {
// 		outputLog(fmt.Sprint(format), Log_Error, 0)
// 	} else {
// 		outputLog(fmt.Sprintf(format, a...), Log_Error, 0)
// 	}
// }

// func outputLog(logstr string, outLogLevel int, pure int) {
// 	if outLogLevel <= Log_Nil || outLogLevel >= Log_Max {
// 		outLogLevel = Log_Error
// 	}

// 	if outLogLevel < logLevel {
// 		return
// 	}

// 	ntime := time.Now()
// 	datetime := fmt.Sprintf("%4d-%02d-%02d %02d:%02d:%02d", ntime.Year(), ntime.Month(),
// 		ntime.Day(), ntime.Hour(), ntime.Minute(), ntime.Second())

// 	var str string
// 	if pure != 0 {
// 		str = fmt.Sprintf("%s|%v", datetime, logstr)
// 	} else {
// 		funcName, file, line, ok := runtime.Caller(2)
// 		if ok {
// 			str = fmt.Sprintf("%s|%s|%s:%d|%s|%v", datetime, ll2str[outLogLevel], filepath.Base(file), line, runtime.FuncForPC(funcName).Name(), logstr)
// 			// logstr = fmt.Sprintf("%s:%d|%s|%v", filepath.Base(file), line, runtime.FuncForPC(funcName).Name(), logstr)
// 		} else {
// 			str = fmt.Sprintf("%s|%s|...|%v", datetime, ll2str[outLogLevel], logstr)
// 		}
// 	}
// 	// 加入goroutine id
// 	str = fmt.Sprintf("%s|gid:[%v]", str, getGID())

// 	if outLogLevel == Log_Error {
// 		if PodName == "" {
// 			PodName = os.Getenv("HOSTNAME")
// 		}
// 		var t LogdefMsg
// 		t.Time = time.Now().Format("2006-01-02T15:04:05Z")
// 		t.Index = "cfwserverMonitor_logdef_err"
// 		t.Message = str
// 		t.PodName = PodName
// 		data, err := json.Marshal(t)
// 		fmt.Sprintf("error %v data is %v", err, data)
// 	}

// 	// if logfile != nil && fileLogon {
// 	// 	if bRollFile() {
// 	// 		rollFile()
// 	// 	}
// 	// 	logfile.Println(str)
// 	// }
// 	// if logshell != nil && shellLogon {
// 	// 	logshell.Println(str)
// 	// }

// 	// atomic.AddInt64(&curLogSize, int64(len(str)))

// }

// // 返回goroutine id
// func getGID() uint64 {
// 	b := make([]byte, 64)
// 	b = b[:runtime.Stack(b, false)]
// 	str := string(b)
// 	str = str
// 	b = bytes.TrimPrefix(b, []byte("goroutine "))
// 	b = b[:bytes.IndexByte(b, ' ')]
// 	n, _ := strconv.ParseUint(string(b), 10, 64)
// 	return n
// }
