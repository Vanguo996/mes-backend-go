## 使用grpc以及protubuf传输来自测量仪器的数据

```
// 服务端流模式，在返回值前面加上一个 stream
  rpc GetStream(StreamRequestData) returns (stream StreamResponseData){}

  // 客户端流模式，在参数前面加上一个 stream
  rpc PutStream(stream StreamRequestData) returns (StreamResponseData){}
  // 双向流模式
  rpc AllStream(stream StreamRequestData) returns (stream StreamResponseData){}

```

protoc安装

```
wget https://github.com/protocolbuffers/protobuf/releases/download/v3.19.1/protoc-3.19.1-linux-x86_64.zip

unzip protoc-3.19.1-linux-x86_64.zip

mv bin/protoc /usr/local/bin/protoc

go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u google.golang.org/grpc


```

通过proto工具生成代码

go:

```
protoc --go_out=plugins=grpc:pb -I . ./sweep.proto

```

error

> protoc-gen-go: program not found or is not executable

解决办法

```
cp go/bin/protoc-gen-go /usr/local/bin
```

python:

```
python3 -m grpc_tools.protoc --python_out=. --grpc_python_out=. -I.. ../sweep.proto
```

## python-streaming-server

python部分结构：

```
├── py-server
│   ├── client.py
│   ├── config.py
│   ├── deploy-server.yaml
│   ├── Dockerfile
│   ├── mes
│   ├── __pycache__
│   ├── requirements.txt
│   ├── server.py
│   ├── sweep_pb2_grpc.py
│   ├── sweep_pb2.py
│   ├── sweep.py
│   ├── van-env
│   └── visa-driver
```

运行：

```
source van-env/bin/activate
python server.py
```

使用yield关键字在循环中返回数据

```python
yield response(result=measure_i_info)
```

#### 容器化部署

```
docker build -t pyvisa-server .


docker run --name pyvisa-server --privileged --cap-add=ALL --net=host -d \
       -v /var/lib/dkms:/var/lib/dkms  -v /lib/modules:/lib/modules -v /usr/src:/usr/src pyvisa-server

```

## golang-client-for-python (also sever for react websockets)

golang部分项目结构

```
├── client.go
├── Dockerfile
├── go.mod
├── go.sum
├── pb
│   └── sweep.pb.go
├── readme.md
└── sweep.proto
```

在golang服务端中，从protubuf中拿到client

```golang
  conn, err := grpc.Dial(addr, grpc.WithInsecure())
	client := pb.NewInstrumentsControllerClient(conn)
```

client调用对应的接口：

```golang
res, err := client.IVSweepMode(context.Background(), req)

```


### 运行react  golang以及python应用

react应用打包
```
docker build -t registry.gitlab.com/vanguo996/mes-web:latest . 

docker run --name mes -p 3000:3000 -d registry.gitlab.com/vanguo996/mes-web:latest
```


golang
```
docker build -t registry.gitlab.com/vanguo996/mes-backend-go:latest .

docker run --name websockets-server --net=host -p 8080:8080 -d registry.gitlab.com/vanguo996/mes-backend-go:latest
```


python

```
docker build -t registry.gitlab.com/vanguo996/mes-backend-pyvisa .

--privileged


不需要挂载 宿主机的文件系统
docker run --name grpc-server --net=host --privileged -p 52001:52001 -d registry.gitlab.com/vanguo996/mes-backend-pyvisa:latest


容器中安装驱动程序，然后启动

docker run --name grpc-server --net=host --privileged -p 52001:52001 -d mes-py-with-visa

成功加载驱动，启动容器。

```


## 容器中不安装驱动程序，直接共享宿主机的visalib，

docker run --name grpc-server-novisa --net=host --privileged  -p 52001:52001 -d -v /usr/lib:/usr/lib -v /var/lib/dkms:/var/lib/dkms -v /lib/modules:/lib/modules mes-py

出现错误：

Traceback (most recent call last):
  File "server.py", line 168, in <module>
    pb2_grpc.add_InstrumentsControllerServicer_to_server(InstrumentsServer(), grpc_server)
  File "server.py", line 19, in __init__
    self.rm = visa.ResourceManager(config.visa_lib)
  File "/usr/local/lib/python3.6/site-packages/pyvisa/highlevel.py", line 3024, in __new__
    obj.session, err = visa_library.open_default_resource_manager()
  File "/usr/local/lib/python3.6/site-packages/pyvisa/ctwrapper/functions.py", line 1871, in open_default_resource_manager
    ret = library.viOpenDefaultRM(byref(session))
  File "/usr/local/lib/python3.6/site-packages/pyvisa/ctwrapper/highlevel.py", line 222, in _return_handler
    return self.handle_return_value(session, ret_value)  # type: ignore
  File "/usr/local/lib/python3.6/site-packages/pyvisa/highlevel.py", line 251, in handle_return_value
    raise errors.VisaIOError(rv)
pyvisa.errors.VisaIOError: VI_ERROR_SYSTEM_ERROR (-1073807360): Unknown system error (miscellaneous error).


pp obj
<ResourceManager(<IVIVisaLibrary('/usr/lib/x86_64-linux-gnu/libvisa.so.20.0.0')>)>


重点在 session 以及 ret返回错误，打开资源管理器失败

```
def open_default_resource_manager(library):
    """This function returns a session to the Default Resource Manager resource.

    Corresponds to viOpenDefaultRM function of the VISA library.

    Returns
    -------
    VISARMSession
        Unique logical identifier to a Default Resource Manager session
    constants.StatusCode
        Return value of the library call.

    """
    session = ViSession()
    ret = library.viOpenDefaultRM(byref(session))
    return session.value, ret

```


其中查看说明书
http://www.ivifoundation.org/docs/vpp43.pdf

此方法在所有操作前都需要调用。
The viOpenDefaultRM() function SHALL be invoked before any operation in VISA. 

传递参数：sesn  类型  ViSession 

> Unique logical identifier to a Default Resource Manager session


返回参数  ViStatus

> This is the operational return status. It returns either a completion code or an error code as follows.






```
docker run --name grpc2 --net=host --privileged  -p 52001:52001 -d -v /usr/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu/ -v /etc/ni-visa:/etc/ni-visa  mes-py

```

```
docker run --name grpc4 --net=host  --privileged -p 52001:52001 -d -v /usr/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:ro -v /etc/ni-visa:/etc/ni-visa:ro  mes-py

特权容器谨慎使用，在容器访问visa驱动的场景下，只使用到了一个功能，即加载内核模块的能力：所以把privilege替换成--cap-add=SYS_MODULE

docker run --name grpc5 --net=host --cap-add=SYS_MODULE -p 52001:52001 -d -v /usr/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:ro -v /etc/ni-visa:/etc/ni-visa:ro mes-py


```

修改dockerfile，打包推送至 gitlab

```
docker run --name grpc5 --net=host --cap-add=SYS_MODULE -p 52001:52001 -d -v /usr/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:ro -v /etc/ni-visa:/etc/ni-visa:ro registry.gitlab.com/vanguo996/mes-backend-pyvisa
```



