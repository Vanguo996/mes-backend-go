  /* KEY `type_value` (`type_value`), */

CREATE TABLE `user_info_test` (
	`user_name` VARCHAR(255),
	`email` VARCHAR(255),
	`user_id` INT NOT NULL AUTO_INCREMENT,
	`password` VARCHAR(255),
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_unique` (`user_id`, `user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户数据';