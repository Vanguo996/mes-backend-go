web服务是一个软件系统，目的是为网络上进行可以互操作
机器间交互提供支持

web服务都有一套自己的接口，被称为WSDL，

web服务还分为不同类型：

- SOAP

- REST

- XML-RPC

SOAP的web服务过于笨重、复杂

SOAP的XML报文会非常冗长，导致难以调试，基于SOAP的web服务会因为资源损耗而无法运行

REST本身不是一种结构，而是一种设计理念

基于REST的web服务使用JSON这种比较简单的数据格式。

两者区别在于：

- SOAP是功能驱动的，RPC风格

- REST是数据驱动的， 关注的是资源，HTTP方法则是对资源操作的动词



## cloud native go

云原生应用应该支持巨大的流量，如果希望在互联网上部署能够有数以千计以及百万人使用，就需要在架构上
做调整，单纯的扩充应用程序的实例是不够的

- 事件溯源(Event Souring)

- CQRS (Command Query Resiponsibility Segregation) 命令查询职责分离 模式

这两种模式都可以解决巨大请求量和吞吐量场景下的应用程序响应问题



### 


```
Before testing the frontend application, 
you will need to make sure that the backend services 
(more precisely, boththe event service and the booking 
service) support Cross-Origin Resource Sharing (CORS). 
Otherwise, yourbrowser will not execute HTTP requests 
to any of your backend services,

when the frontend is served on http://localhost:8080 and 
the backend services run on other TCP ports

 the HTTP response needs to contain 
 an Access-Control-Allow-Origin header

```


```go
credentials := handlers.AllowCredentials()

if err := http.ListenAndServe(constant.LoginServerPort, handlers.CORS()(router)); err != nil {
    logdef.Log.Fatal(err)
}


func main() {
   router := newREST()
   credentials := handlers.AllowCredentials()
   methods := handlers.AllowedMethods([]string{"POST"})
   ttl := handlers.MaxAge(3600)
   origins := handlers.AllowedOrigins([]string{"www.example.com"})
   log.Fatal(http.ListenAndServe(":5000", handlers.CORS(credentials, methods, origins)(router)))
}

```



## golang 摄像头实时传输视频数据

```go
go get "github.com/blackjack/webcam"

cam, err := webcam.Open("/dev/video0") // Open webcam

```

> Open a webcam with a given path Checks if device is a 
> v4l2 device and if it is capable to stream video


插入摄像头，查看设备：
```
ls /dev 

v4l 
video0
```

### 摄像头配置

查看支持格式
```go

	// select pixel format
	format_desc := cam.GetSupportedFormats()

	fmt.Println("Available formats:")
	for _, s := range format_desc {
		fmt.Fprintln(os.Stderr, s)
	}


Available formats:
YUV 4:2:2 (YUYV)

```




### 登录注册服务

db root password： reactgo

```
docker run --name mes_db -p 7890:3306 -e MYSQL\_ROOT\_PASSWORD=mes@2021 -d mysql:5.7

mysql -uroot -pmes@2021

use measurement_system;

```

gormt - mysql database to golang struct conversion tools

```
go get -u -v github.com/xxjwxc/gormt@master

./gormt -g=true

```

```
	// SELECT * FROM users;
	// results := newDb.Find(&models.MesUserInfo{})

```

主要功能：

- 登录

获取用户名密码

创建jwt

jwt放入cookie中

- 注册
- 注销

其中需要 鉴权的接口：

使用 `PathPrefix`检查token
