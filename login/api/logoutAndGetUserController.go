package api

import (
	"measurement-backend/common"
	"measurement-backend/common/conf"
	db "measurement-backend/common/db"
	"measurement-backend/common/logdef"
	"measurement-backend/common/models"
	"net/http"
	"strings"
	"time"
	// db "measurement-backend/common/db"
	// "measurement-backend/common/models"
)

type LogoutRequest struct {
	UserName string `json:"userName"`
}

type GetUserRequest struct {
}

type LogoutResult struct {
	Role string `json:"role"`
}

// 登出控制器
func LogoutController() common.MesApiInterface {
	return &LogoutRequest{}
}

// 获取用户控制器
func GetUseController() common.MesApiInterface {
	return &GetUserRequest{}
}

func (req *LogoutRequest) MesGetResult(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	cookie := http.Cookie{
		Name:    "token",
		Value:   "",
		Expires: time.Now().Add(-time.Hour * 24),
	}
	http.SetCookie(w, &cookie)

	return conf.LogoutSuccess, nil
}

func (req *GetUserRequest) MesGetResult(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	cookie, err := r.Cookie("token")

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		logdef.Log.Errorf("no cookie StatusUnauthorized: %v", err)
		return http.StatusUnauthorized, err
	}
	// token=<jwt>
	tokenString := strings.Split(cookie.String(), "=")
	if len(tokenString) != 2 {
		logdef.Log.Errorf("tokenString error : %v", err)
		return nil, err
	}

	claims, err := ParseToken(tokenString[1])
	if err != nil {
		logdef.Log.Errorf("ParseToken error : %v", err)
	}

	logdef.Log.Infof("token is: %v", claims)

	var userInfo models.UserInfoTest

	if err := db.GetMesClient().Model(&models.UserInfoTest{}).Select("*").
		Where("user_name = ?", claims.Username).
		Find(&userInfo).Error; err != nil {
		logdef.Log.Errorf("error getting user info: %v", err)
	}

	return userInfo, nil

}
