package api

import (
	"measurement-backend/common"
	"measurement-backend/common/conf"
	db "measurement-backend/common/db"
	"measurement-backend/common/logdef"
	"measurement-backend/common/models"
	"net/http"
)

type RegisterRequest struct {
	UserName        string `json:"userName"`
	Email           string `json:"email"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"passwordConfirm"`
}

type RegisterResult struct {
	Token string `json:"token"`
	Role  string `json:"role"`
}

func RegisterController() common.MesApiInterface {
	return &RegisterRequest{}
}

func (req *RegisterRequest) MesGetResult(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	logdef.Log.Info(req)

	if ValidateUser(req) {
		logdef.Log.Info("已经注册")
		return conf.AlreadyRegisterd, nil
	}

	cryptedPasswd, err := GetHashedPassword(req.Password)
	if err != nil {
		logdef.Log.Errorf("GetHashedPassword %v", err)
		return nil, err
	}

	userRecord := &models.UserInfoTest{
		UserName: req.UserName,
		Password: cryptedPasswd,
		Email:    req.Email,
	}

	res := db.GetMesClient().Create(userRecord)
	if res.Error != nil {
		logdef.Log.Errorf("Create userRecord error: %v", err)
		return nil, err
	}

	logdef.Log.Info("注册完成")

	// allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"

	// if origin := r.Header.Get("Origin"); origin != "" {
	// 	w.Header().Set("Access-Control-Allow-Origin", "*")
	// 	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	// 	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	// 	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	// }

	return conf.RegisterSuccess, nil

}
