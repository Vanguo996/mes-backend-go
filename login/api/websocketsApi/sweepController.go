package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"measurement-backend/common"
	"measurement-backend/common/conf"
	"measurement-backend/common/logdef"
	"measurement-backend/constant"
	"strconv"
	"strings"

	pb "measurement-backend/pb"

	"github.com/gorilla/websocket"
	"google.golang.org/grpc"
)

type sweRequest struct {
	StartVolt   float32 `json:"startVolt"`
	EndVolt     float32 `json:"endVolt"`
	VoltStep    float32 `json:"voltStep"`
	CurrentCmpl float32 `json:"currentCmpl"`
	MesSpeed    float32 `json:"mesSpeed"`
}

type sweResult struct {
	Voltage    float64 `json:"voltage"`
	Current    float64 `json:"current"`
	Timestamp  float64 `json:"timestamp"`
	Resistance float64 `json:"resistance"`
}

func SweController() common.WebsocketsInterface {
	return &sweRequest{}
}

func (req *sweRequest) WsGetResult(connection *websocket.Conn) (interface{}, error) {
	var (
		volt, curr, timestamp, resistance float64
		err                               error
	)

	go func() {
		for {
			_, payload, err := connection.ReadMessage()
			if err != nil {
				logdef.Log.Error(err)
				break
			}
			if payload != nil {
				logdef.Log.Infof("payload: %s", string(payload))
			}
		}
	}()

	// 请求grpc接口
	logdef.Log.Infof("请求grpc server：%s", constant.HostAddr)
	conn, err := grpc.Dial(constant.HostAddr, grpc.WithInsecure())
	if err != nil {
		logdef.Log.Error(err)
		connection.WriteMessage(conf.GrpcServerConnectionError, []byte(err.Error()))
		connection.Close()
		return nil, err
	}

	defer conn.Close()

	// 新建一个grpc客户端
	client := pb.NewInstrumentsControllerClient(conn)

	sweReq := &pb.SweepRequest{
		StartVolt:   -1,
		EndVolt:     1,
		VoltStep:    0.2,
		CurrentCmpl: 1,
		MesSpeed:    5,
	}


	// sweReq := &pb.SweepRequest{
	// 	StartVolt:   req.StartVolt,
	// 	EndVolt:     req.EndVolt,
	// 	VoltStep:    req.VoltStep,
	// 	CurrentCmpl: req.CurrentCmpl,
	// 	MesSpeed:    req.MesSpeed,
	// }

	logdef.Log.Info(sweReq)

	sweepRawData, err := client.IVSweepMode(context.Background(), sweReq)

	if err != nil {
		logdef.Log.Error(err)
		connection.WriteMessage(conf.GrpcServerConnectionError, []byte(err.Error()))
		return nil, err
	}

	logdef.Log.Info("sweep mode开始")
	for {

		res, err := sweepRawData.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		// 数据分析
		mesRes := res.Result
		arr := strings.Split(mesRes, ",")
		//返回的数据格式为：volt. curr, none, time, none

		if len(arr) >= 5 {
			volt, err = strconv.ParseFloat(arr[0], 64)
			curr, err = strconv.ParseFloat(arr[1], 64)
			timestamp, err = strconv.ParseFloat(arr[3], 64)
			if err != nil {
				logdef.Log.Error(err)
			}
			resistance = volt / curr
		}
		fmt.Println(timestamp, resistance, volt, curr)
		sweRes := &sweResult{
			Voltage:    volt,
			Current:    curr,
			Timestamp:  timestamp,
			Resistance: resistance,
		}
		resBytes, _ := json.Marshal(sweRes)

		err = connection.WriteMessage(1, resBytes)
		if err != nil {
			logdef.Log.Error(err)
		}

	}

	logdef.Log.Info("消息发送完毕")
	// client.CloseOutput(context.Background())

	return nil, nil
}
