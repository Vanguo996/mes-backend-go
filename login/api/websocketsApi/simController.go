package api

import (
	"context"
	"io"
	"measurement-backend/common"
	"measurement-backend/common/conf"
	"measurement-backend/common/logdef"
	"measurement-backend/constant"

	pb "measurement-backend/pb"

	"github.com/gorilla/websocket"
	"google.golang.org/grpc"
)

type simRequest struct {
	Name string `json:"name"`
}

// type simResult struct {
// }

func SimController() common.WebsocketsInterface {
	return &simRequest{}
}

func (req *simRequest) WsGetResult(connection *websocket.Conn) (interface{}, error) {

	go func() {
		for {
			_, payload, err := connection.ReadMessage()
			if err != nil {
				logdef.Log.Error(err)
				break
			}
			if payload != nil {
				logdef.Log.Infof("payload: %s", string(payload))
			}
		}
	}()

	// 请求grpc接口
	logdef.Log.Infof("请求grpc server：%s", constant.HostAddr)
	conn, err := grpc.Dial(constant.HostAddr, grpc.WithInsecure())
	if err != nil {
		logdef.Log.Error(err)
		connection.WriteMessage(conf.GrpcServerConnectionError, []byte(err.Error()))
		connection.Close()
		return nil, err
	}

	defer conn.Close()

	// 新建一个grpc客户端
	client := pb.NewInstrumentsControllerClient(conn)

	simReq := &pb.SimRequest{
		StartVolt: 0.2,
	}

	res, err := client.SimulationMode(context.Background(), simReq)

	// sweReq := &pb.SweepRequest{
	// 	StartVolt:   -2,
	// 	EndVolt:     2,
	// 	VoltStep:    0.1,
	// 	CurrentCmpl: 100e-3,
	// }
	// res, err := client.IVSweepMode(context.Background(), sweReq)

	if err != nil {
		logdef.Log.Error(err)
		connection.WriteMessage(conf.GrpcServerConnectionError, []byte(err.Error()))
		return nil, err
	}

	logdef.Log.Info("消息开始发送")
	for {

		res, err := res.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		err = connection.WriteMessage(1, []byte(res.Result))
		if err != nil {
			logdef.Log.Error(err)
		}

	}

	logdef.Log.Info("消息发送完毕")

	return nil, nil
}
