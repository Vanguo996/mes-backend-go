package api

import (
	"bytes"

	"measurement-backend/common"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"os"
	"sort"
	"strconv"
	"time"

	"fmt"
	"measurement-backend/common/logdef"

	"github.com/blackjack/webcam"
)

const (
	V4L2_PIX_FMT_PJPG = 0x47504A50
	V4L2_PIX_FMT_YUYV = 0x56595559
)

type FrameSizes []webcam.FrameSize

func (slice FrameSizes) Len() int {
	return len(slice)
}

//For sorting purposes
func (slice FrameSizes) Less(i, j int) bool {
	ls := slice[i].MaxWidth * slice[i].MaxHeight
	rs := slice[j].MaxWidth * slice[j].MaxHeight
	return ls < rs
}

//For sorting purposes
func (slice FrameSizes) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

var supportedFormats = map[webcam.PixelFormat]bool{
	V4L2_PIX_FMT_PJPG: true,
	V4L2_PIX_FMT_YUYV: true,
}

// var device = flag.String("input", "/dev/video0", "Input video device")

type videoRequest struct {
}

type videoResult struct {
	Token string `json:"token"`
	Role  string `json:"role"`
}

func VideoController() common.MesApiInterface {
	return &videoRequest{}
}

func (req *videoRequest) MesGetResult(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	logdef.Log.Info("ok")

	// fmtstr := flag.String("f", "", "video format to use, default first supported")
	// szstr := flag.String("s", "", "frame size to use, default largest one")

	// fps := flag.Bool("p", false, "print fps info")
	// flag.Parse()

	cam, err := webcam.Open("/dev/video0")
	if err != nil {
		panic(err.Error())
	}
	defer cam.Close()

	// select pixel format
	format_desc := cam.GetSupportedFormats()

	fmt.Println("Available formats:")
	for _, s := range format_desc {
		fmt.Fprintln(os.Stderr, s)
	}

	var format webcam.PixelFormat

	// FMT:
	// for f, s := range format_desc {
	// 	if *fmtstr == "" {

	// 	} else if *fmtstr == s {
	// 		if !supportedFormats[f] {
	// 			logdef.Log.Println(format_desc[f], "format is not supported, exiting")
	// 			return nil, nil
	// 		}
	// 		format = f
	// 		break
	// 	}
	// }

	for f, _ := range format_desc {

		if supportedFormats[f] {
			format = f
		}

	}

	if format == 0 {
		logdef.Log.Println("No format found, exiting")
		return nil, nil
	}

	// select frame size
	frames := FrameSizes(cam.GetSupportedFrameSizes(format))
	sort.Sort(frames)

	fmt.Fprintln(os.Stderr, "Supported frame sizes for format", format_desc[format])
	for _, f := range frames {
		fmt.Fprintln(os.Stderr, f.GetString())
	}
	var size *webcam.FrameSize

	//默认选取最大分辨率
	size = &frames[len(frames)-1]

	// if *szstr == "" {

	// } else {
	// 	for _, f := range frames {
	// 		if *szstr == f.GetString() {
	// 			size = &f
	// 		}
	// 	}
	// }

	if size == nil {
		logdef.Log.Println("No matching frame size, exiting")
		return nil, nil
	}

	fmt.Fprintln(os.Stderr, "Requesting", format_desc[format], size.GetString())
	f, width, h, err := cam.SetImageFormat(format, uint32(size.MaxWidth), uint32(size.MaxHeight))
	if err != nil {
		logdef.Log.Println("SetImageFormat return error", err)
		return nil, nil

	}
	fmt.Fprintf(os.Stderr, "Resulting image format: %s %dx%d\n", format_desc[f], width, h)

	// start streaming
	err = cam.StartStreaming()
	if err != nil {
		logdef.Log.Println(err)
		return nil, nil
	}

	logdef.Log.Print("开始流")

	var (
		li   chan *bytes.Buffer = make(chan *bytes.Buffer)
		fi   chan []byte        = make(chan []byte)
		back chan struct{}      = make(chan struct{})
	)

	go httpVideo(li, w, r)

	timeout := uint32(5) //5 seconds
	// start := time.Now()

	var fr time.Duration

	for {
		err = cam.WaitForFrame(timeout)
		if err != nil {
			logdef.Log.Println(err)
			return nil, nil
		}

		switch err.(type) {
		case nil:
		case *webcam.Timeout:
			logdef.Log.Println(err)
			continue
		default:
			logdef.Log.Println(err)
			return nil, nil
		}

		frame, err := cam.ReadFrame()
		if err != nil {
			logdef.Log.Println(err)
			return nil, nil
		}
		if len(frame) != 0 {

			// print framerate info every 10 seconds
			fr++

			// if *fps {
			// 	if d := time.Since(start); d > time.Second*10 {
			// 		fmt.Println(float64(fr)/(float64(d)/float64(time.Second)), "fps")
			// 		start = time.Now()
			// 		fr = 0
			// 	}
			// }

			select {
			case fi <- frame:
				<-back
			default:
			}
		}
	}

}

func httpVideo(li chan *bytes.Buffer, w http.ResponseWriter, r *http.Request) {
	// http.HandleFunc("/video", func() {

	logdef.Log.Println("connect from", r.RemoteAddr, r.URL)

	//remove stale image
	<-li
	const boundary = `frame`
	w.Header().Set("Content-Type", `multipart/x-mixed-replace;boundary=`+boundary)
	multipartWriter := multipart.NewWriter(w)
	multipartWriter.SetBoundary(boundary)

	for {
		img := <-li
		image := img.Bytes()
		iw, err := multipartWriter.CreatePart(textproto.MIMEHeader{
			"Content-type":   []string{"image/jpeg"},
			"Content-length": []string{strconv.Itoa(len(image))},
		})
		if err != nil {
			logdef.Log.Println(err)
			return
		}
		_, err = iw.Write(image)
		if err != nil {
			logdef.Log.Println(err)
			return
		}
	}
}
