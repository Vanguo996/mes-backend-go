package api

import (
	"measurement-backend/common/conf"
	db "measurement-backend/common/db"
	"measurement-backend/common/logdef"
	"measurement-backend/common/models"
	"net/http"
	"os"
	"time"

	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"
)

//Claim是一些实体（通常指的用户）的状态和额外的元数据
type MesClaims struct {
	Username string `json:"username"`
	Password string `json:"password"`
	jwt.StandardClaims
}

func HelloWord(w http.ResponseWriter, r *http.Request) {
	b := []byte("hello world!")
	w.Write(b)
}

func GetHashedPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func CreateToken(userName string, password string) (string, error) {
	//创建签名密钥
	err := os.Setenv(conf.ACCESS_SECRET_ENV_NAME, conf.ACCESS_SECRET)
	if err != nil {
		logdef.Log.Error("set ACCESS_SECRET error: %v", err)
	}

	claims := MesClaims{
		Username: userName,
		Password: password,
		StandardClaims: jwt.StandardClaims{
			// 过期时间
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
			// 指定token发行人
			Issuer: "mes-server",
		},
	}

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// 产生token
	token, err := at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return "", err
	}
	return token, nil
}

// // 解析token
// func ExtractToken(r *http.Request) string {
// 	bearToken := r.Header.Get("Authorization")
// 	//normally Authorization the_token_xxx
// 	strArr := strings.Split(bearToken, " ")
// 	if len(strArr) == 2 {
// 		return strArr[1]
// 	}
// 	return ""
// }

// 验证用户是否存在
func ValidateUser(r *RegisterRequest) bool {
	var info models.UserInfoTest

	err := db.GetMesClient().Model(&models.UserInfoTest{}).
		Select("user_name").
		Where("user_name = ?", r.UserName).
		Limit(1).
		Find(&info).
		Error
	logdef.Log.Info(info.UserName)
	return err == nil
}

// 解析token
func ParseToken(tokenString string) (*MesClaims, error) {
	// MesClaims使用自定义的claims解密
	token, err := jwt.ParseWithClaims(tokenString, &MesClaims{}, func(t *jwt.Token) (interface{}, error) {
		//通过密钥解密
		return []byte(conf.ACCESS_SECRET), nil
	})
	if err != nil {
		logdef.Log.Errorf("ParseWithClaims error: %v", err)
		return nil, err
	}

	if claims, ok := token.Claims.(*MesClaims); ok {
		return claims, nil
	}
	return nil, err
}

func CheckToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here
		token := r.FormValue("token")
		// ParseToken()
		if token == "xueyuanjun.com" {
			logdef.Log.Infof("Token check success: %s\n", r.RequestURI)
			// Call the next handler, which can be another middleware in the chain, or the final handler.
			next.ServeHTTP(w, r)
		} else {
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
}

// 验证用户
func LoginValidateUser(r *LoginRequest) bool {
	var info models.UserInfoTest

	err := db.GetMesClient().Model(&models.UserInfoTest{}).
		Select("user_name").
		Where("user_name = ?", r.UserName).
		Limit(1).
		Find(&info).
		Error
	return err == nil
}
