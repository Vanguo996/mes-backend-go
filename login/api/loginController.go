package api

import (
	"measurement-backend/common"
	"measurement-backend/common/conf"
	db "measurement-backend/common/db"
	"measurement-backend/common/logdef"
	"measurement-backend/common/models"
	"net/http"
)

type LoginRequest struct {
	UserName string `json:"userName"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type MesLoginResult struct {
	Token string `json:"token"`
	Role  string `json:"role"`
}

func LoginController() common.MesApiInterface {
	return &LoginRequest{}
}

func (req *LoginRequest) MesGetResult(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	var (
		userInfo models.UserInfoTest
	)

	logdef.Log.Info(req)

	if !LoginValidateUser(req) {
		logdef.Log.Info("用户未注册")
		return conf.UserNotExist, nil
	}

	err := db.GetMesClient().Model(&models.UserInfoTest{}).
		Select("user_id, password").
		Where("user_name = ?", req.UserName).
		Limit(1).
		Find(&userInfo).Error
	if err != nil {
		logdef.Log.Errorf("login info error: %v", err)
		return nil, err
	}

	if CheckPasswordHash(req.Password, userInfo.Password) {
		logdef.Log.Info("login success")
		// 返回jwt
		jwt_token, err := CreateToken(req.UserName, req.Password)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			logdef.Log.Error("create token error: %v", err)
		}

		http.SetCookie(w, &http.Cookie{
			Name:  "token",
			Value: jwt_token,
			// Expires:  time.Now().Add(time.Hour * 24),
			HttpOnly: true,
		})
		return conf.LoginSuccess, nil
	} else {
		// result = conf.LoginFailed
		logdef.Log.Error("login failed")
		return conf.LoginFailed, err
	}

}
