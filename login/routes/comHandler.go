package routes

import (
	common "measurement-backend/common"
	"measurement-backend/common/conf"
	"net/http"

	"measurement-backend/common/logdef"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

// 升级请求头
func Upgrader(rw http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	logdef.Log.Info("升级请求头...")
	conn, err := upgrader.Upgrade(rw, r, nil)
	if err != nil {
		logdef.Log.Println(err)
		return conn, err
	}
	return conn, nil
}

// 处理请求
func APICommonHandle(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()
	defer common.HandleError(w)

	path := r.URL.Path
	controller := getControllerFromPath(path, w)
	logdef.Log.Infof("请求路径： %s", path)

	MesGetControllerHanlder(w, r, controller.MesGetController())
}

//处理websockets请求
func WebsocketApiHandler(w http.ResponseWriter, r *http.Request) {

	var (
		err error
	)

	defer r.Body.Close()
	defer common.HandleError(w)

	path := r.URL.Path
	// 通过path寻找controller
	config, ok := WebsocketsApiConfig[path]
	if !ok {
		logdef.Log.Errorf("no such path error: %s", path)
		common.SendResult(w, conf.SerErr, err.Error(), nil)
	}
	logdef.Log.Infof("请求路径： %s", path)
	logdef.Log.Info(r.URL)

	// 升级请求头
	connection, err := Upgrader(w, r)
	if err != nil {
		logdef.Log.Println(err)
		return
	}

	controller := config.WebsocketsController()
	// 传入websockets连接
	controller.WsGetResult(connection)

	// common.SendResult(w, 0, "test", "WebsocketApiHandler send")

}

func getControllerFromPath(path string, w http.ResponseWriter) common.MesApiController {
	var (
		err error
	)
	// 匹配路由
	config, ok := APIControllerConfig[path]
	if !ok {
		logdef.Log.Fatal("no path error %s", path)
		common.SendResult(w, conf.SerErr, err.Error(), nil)
	}
	return config
}

func MesGetControllerHanlder(w http.ResponseWriter, r *http.Request, controller common.MesApiInterface) {
	var (
		result interface{}
		err    error
	)

	if r.Method == http.MethodPost {
		verifyBody(w, r, controller)
	}

	// 获取结果
	if result, err = controller.MesGetResult(w, r); err != nil {
		logdef.Log.Errorf("MesGetResult: error = %s ", err)
		// 返回出错信息
		common.SendResult(w, conf.SerErr, err.Error(), nil)
		return
	}

	common.SendResult(w, conf.CodeSuccess, conf.MsgOK, result)
}

func verifyBody(w http.ResponseWriter, r *http.Request, controller common.MesApiInterface) {
	var err error

	if err = common.StoreRequestBody(r, controller); err != nil {
		logdef.Log.Error("StoreRequestBody: error = %s ", err)
		common.SendResult(w, conf.JSONErr, err.Error(), nil)
		return
	}

	// 校验
	if err = common.ValidParam(controller); err != nil {
		logdef.Log.Errorf("ValidParam: error = %s ", err)
		common.SendResult(w, conf.JSONErr, err.Error(), nil)
		return
	}
}

// // //非登陆操作校验Token并且获取用户信息
// var userInfo *models.MesUserInfo = &models.MesUserInfo{
// 	ReturnCode: conf.CodeSuccess,
// }
// var returnCode int = conf.CodeSuccess

// // 获取结果
// if result, err = controller.MesGetResult(userInfo); err != nil {
// 	log.Errorf("MesGetResult: error = %s ", err)
// 	returnCode = conf.SerErr
// 	if userInfo.ReturnCode != conf.CodeSuccess {
// 		returnCode = userInfo.ReturnCode
// 	}
// 	if userInfo.ReturnCode == conf.UserNotExist {
// 		common.SendResult(w, returnCode, err.Error(), result)
// 	} else {
// 		common.SendResult(w, returnCode, err.Error(), nil)
// 	}

// 	return
// }
