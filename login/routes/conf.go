package routes

const (
	mesLogin    = "/api/mesLogin"
	mesRegister = "/api/mesRegister"
	mesLogout   = "/api/mesLogout"
	getUser     = "/api/getUser"

	getVideo = "/api/getVideo"

	// websockets接口
	simDtataApi = "/ws/test"
	sweApi      = "/ws/swe"
)
