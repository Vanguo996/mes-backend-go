package routes

import (
	common "measurement-backend/common"
	"measurement-backend/login/api"
	wsApi "measurement-backend/login/api/websocketsApi"
)

var (
	// api接口配置，key为url的path
	APIControllerConfig = map[string]common.MesApiController{
		// 注册接口
		mesRegister: {
			MesGetController: api.RegisterController,
		},
		// 登出
		mesLogout: {
			MesGetController: api.LogoutController,
		},
		getUser: {
			MesGetController: api.GetUseController,
		},

		mesLogin: {
			MesGetController: api.LoginController,
		},

		// 视频接口
		getVideo: {
			MesGetController: api.VideoController,
		},
	}

	// websockets接口
	// 测试数据
	WebsocketsApiConfig = map[string]common.WebsocketsApiController{
		simDtataApi: {
			WebsocketsController: wsApi.SimController,
		},
		sweApi: {
			WebsocketsController: wsApi.SweController,
		},
	}
)
