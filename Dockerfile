FROM golang:1.15-alpine

WORKDIR /app

COPY . .

RUN go env -w GO111MODULE=on && \
    go env -w GOPROXY=https://goproxy.cn,direct &&\
    go mod download

RUN go build -o /websockets-server

CMD [ "/websockets-server" ]