package constant

const (
	Instr2400 = "visa://202.197.78.43/GPIB0::24::INSTR"
	HostAddr  = "127.0.0.1:52001"
	// HostAddr   = "10.100.2.35:52001"
	DockerAddr = "127.0.0.1:52001"

	// login server
	LoginServerPort = "0.0.0.0:8080"

	// websockets server
	WebsocketServerPort = "0.0.0.0:8081"
)
